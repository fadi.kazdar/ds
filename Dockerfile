FROM openjdk:7-jre-alpine

WORKDIR /app

COPY target ./target
COPY run.sh ./run.sh

RUN chmod a+x run.sh
EXPOSE 8080

CMD ["run.sh"]
